import axios from 'axios';
const Store = window.require('electron-store'); //webpack barfs if this is imported any other way

const store = new Store();
const selectedTeamsKey = 'selectedTeams';

export async function getAllLeagues() {
  let leagues = [];
  try {
    const resp = await axios.get('http://www.goalfeed.ca/get-teams');
    leagues = resp.data;
  } catch (e) {
    console.log(e);
  }

  return leagues;
}

export function setSelectedTeams(selectedTeams) {
  store.set(selectedTeamsKey, selectedTeams);
}
export async function getSelectedTeams() {
  return await store.get(selectedTeamsKey, []);
}

export async function updateSelectedTeams(selectedTeams) {
  console.log(selectedTeams);
}
