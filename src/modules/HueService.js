import huejay from 'huejay';
const Store = window.require('electron-store'); //webpack barfs if this is imported any other way
const store = new Store();

export function disconnectBridge() {
  store.delete('bridge-user');
  store.delete('bridge-ip');
}
export async function saveBridgeUser(user) {
  store.set('bridge-user', user);
}

export async function setBridgeIp(ip) {
  store.set('bridge-ip', ip);
}

export async function getBridgeIp() {
  const ip = await store.get('bridge-ip', false);
  return ip;
}

export function setBridgeUser(user) {
  store.set('bridge-user', user);
  console.log('saved');
}

export async function getBridgeUser() {
  const user = await store.get('bridge-user', false);
  console.log(user);
  return user;
}

export async function connectionIsSetUp() {
  const ip = await getBridgeIp();
  const user = await getBridgeUser();

  return !!ip && !!user;
}

export async function getClient() {
  const ip = await getBridgeIp();
  const user = await getBridgeUser();
  let client = new huejay.Client({
    host: ip,
    username: user.attributes.attributes.username,
  });

  return client;
}

export async function getLights() {
  let client = await getClient();
  let lights = await client.lights.getAll();

  console.log('lights');
  console.log(lights);
  return lights;
}

export async function getGoalfeedGroup() {
  let client = await getClient();
  let groups = await client.groups.getAll();
  let goalfeedGroup = {};

  for (let group of groups) {
    if (group.name === 'goallight') {
      goalfeedGroup = group;
    }
  }
  console.log('goalfeed group');
  console.log(goalfeedGroup);
  return goalfeedGroup;
}

export async function getGoalfeedGroupLightIds() {
  const goalfeedGroup = await getGoalfeedGroup();

  return goalfeedGroup === {} ? {} : goalfeedGroup.lightIds;
}

export async function storeOldStates(user, callback) {
  let client = await getClient();
  let lights = await client.lights.getAll();
  let oldStates = [];

  for (const light of lights) {
    console.log(light);

    oldStates = [
      ...oldStates,
      {
        id: light.id,
        state: {
          hue: light.hue,
          saturation: light.saturation,
          brightness: light.brightness,
          on: light.on,
        },
      },
    ];
  }
  await store.set('prev-state', oldStates);
}

export async function goal() {
  const user = await getBridgeUser();
  if (!user) return;

  await storeOldStates();
  const delay = await getDelay();
  setTimeout(runGoalSequence, delay * 1000);
}

export async function runGoalSequence() {
  const transitionTime = 1; //1 second
  const redState = {
    hue: 150,
    saturation: 255,
    brightness: 255,
    transitionTime,
  };
  const darkState = { brightness: 0, transitionTime };
  let dark = false;
  const group = await getGoalfeedGroup();
  const groupId = group.id;
  console.log(groupId);

  const duration = await getDuration();
  const interval = setInterval(() => {
    if (dark) {
      console.log('goingRed');
      setGroup(groupId, redState);
      dark = false;
    } else {
      console.log('goingDark');
      setGroup(groupId, darkState);
      dark = true;
    }
  }, 1000); //time in millaseconds to wait

  setTimeout(() => {
    stopGoal(interval);
  }, duration * 1000);
  return interval;
}

export async function getDuration(duration) {
  return await store.get('duration', 45);
}
export function setDuration(duration) {
  store.set('duration', duration);
}

export async function getDelay(delay) {
  return await store.get('delay', 0);
}
export function setDelay(delay) {
  store.set('delay', delay);
}

export function stopGoal(interval) {
  clearInterval(interval); //break the interval
  resetLights();
}

export async function resetLights() {
  const oldStates = await store.get('prev-state');
  for (const light of oldStates) {
    setLight(light.id, light.state);
  }
}

export async function setLight(id, state) {
  const client = await getClient();
  let light = await client.lights.getById(id);
  try {
    client.lights.save(Object.assign(light, state));
  } catch (e) {
    console.log({ message: 'Error setting light ' + id, light, error: e });
  }
}

export async function setGroup(id, state) {
  const client = await getClient();
  let group = await client.groups.getById(id);
  try {
    client.groups.save(Object.assign(group, state));
  } catch (e) {
    console.log({ message: 'Error setting group ' + id, group, error: e });
  }
}

export async function setGoalfeedGroup(lightIds) {
  let client = await getClient();
  let group;
  const goalfeedGroup = await getGoalfeedGroup();

  if (goalfeedGroup === {}) {
    group = new client.groups.Group();
    group.name = 'goallight';
    group.lightIds = lightIds;

    group = await client.groups.create(group);
  } else {
    group = await client.groups.getById(goalfeedGroup.id);
    group.lightIds = lightIds;
    client.groups.save(group);
  }

  console.log(`Group [${group.id}] saved`);

  return group;
}
