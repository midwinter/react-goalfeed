import Goalfeed from 'goalfeed';
import EventEmitter from 'events';
const Store = window.require('electron-store'); //webpack barfs if this is imported any other way
const store = new Store();

class GoalfeedService extends EventEmitter {
  static instance = null;

  static createInstance() {
    const instance = new GoalfeedService();
    return instance;
  }

  static getInstance() {
    if (!GoalfeedService.instance) {
      GoalfeedService.instance = GoalfeedService.createInstance();
    }
    return GoalfeedService.instance;
  }

  setCredentials(username, password) {
    this.setUsername(username);
    this.setPassword(password);
    this.reinit();
  }
  setPassword(password) {
    this.password = password;
    store.set('password', this.password);
  }
  setUsername(username) {
    this.username = username;
    store.set('username', this.username);
  }
  async getUsername() {
    return await store.get('username', '');
  }
  async getPassword() {
    return await store.get('password', '');
  }

  constructor(username = null, password = null) {
    super();
    this.username = username;
    this.password = password;
    this.goalfeed = null;
    this.connecting = true;
    this.reinit(true);
  }

  async reinit(initializeCredentials = false) {
    this.connecting = true;
    if (initializeCredentials) {
      this.username = await this.getUsername();
      this.password = await this.getPassword();
    }
    if (!this.username || !this.password) {
      this.goalfeed = null;
    } else {
      this.goalfeed = new Goalfeed(this.username, this.password);
      this.goalfeed.goal_callback = this.onGoal;
    }
    this.connecting = false;
  }

  onGoal = data => {
    this.emit('goal', data);
  };

  isConnecting() {
    return this.goalfeed ? this.goalfeed.connecting : this.connecting;
  }
  isConnected() {
    if (!this.goalfeed) {
      return false;
    }
    return this.goalfeed.is_connected;
  }
}

export default GoalfeedService;
