import React, { Component } from 'react';
import logo from '../images/goalfeed-sm.png';
import MenuItem from './partials/MenuItem';
var pjson = require('../../package.json');

const menuStyle = {
  backgroundColor: '#000',
  //height: "100%",
  //position:"fixed"
};
const versionStyle = {
  bottom: '2px',
  left: '2px',
  position: 'absolute',
};
export class Menu extends Component {
  render() {
    return (
      <div style={menuStyle}>
        <div style={{ margin: '4vh 2vw' }}>
          <img src={logo} alt="Goalfeed Logo" />
        </div>
        <ul className="sideMenu">
          <MenuItem itemText="Lights" icon="lightbulb" to="/lights" />
          <MenuItem itemText="Teams" icon="hockey-puck" to="/teams" />
          <MenuItem itemText="Settings" icon="user-cog" to="/" />
        </ul>
        <div style={versionStyle}>v. {pjson.version}</div>
      </div>
    );
  }
}

export default Menu;
