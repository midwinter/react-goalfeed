import React, { Component } from 'react';
import Checkbox from '../partials/Checkbox.jsx';
import GoalfeedService from '../../modules/GoalfeedService';
import * as HueService from '../../modules/HueService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faLightbulbSlash,
  faLightbulbOn,
} from '@fortawesome/pro-light-svg-icons';
import Feed from '../partials/Feed';

const goalfeedService = GoalfeedService.getInstance();

export class Settings extends Component {
  state = {
    connected: false,
    connecting: false,
    initialEmail: null,
    duration: null,
    delay: null,
    testEvents: false,
  };
  constructor() {
    super();
    this.credsTimeout = null;
    this.connectedInterval = null;
    this.email = null;
    this.password = null;
    this.init();
  }
  init = async () => {
    const email = await goalfeedService.getUsername();
    const password = await goalfeedService.getPassword();
    const delay = await HueService.getDelay();
    const duration = await HueService.getDuration();
    this.email = email;
    this.password = password;
    this.setState({ initialEmail: email, delay, duration });
  };
  checkConnected = () => {
    if (!this.state) {
      return;
    }
    const connecting = goalfeedService.isConnecting();
    if (connecting !== this.state.connecting) {
      this.setState({ connecting });
    }
    const connected = goalfeedService.isConnected();
    if (connected !== this.state.connected) {
      this.setState({ connected });
    }
  };
  componentDidMount() {
    this.connectedInterval = setInterval(this.checkConnected, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.connectedInterval);
  }

  renderConnectedIndicator() {
    if (this.state.connecting) {
      return (
        <div>
          <span className="status">Authenticating...</span>
        </div>
      );
    }
    const connected = this.state.connected ? true : false;
    const indicator = connected ? faLightbulbOn : faLightbulbSlash;
    return (
      <div>
        <span className="indicator">
          <FontAwesomeIcon className="icoLightChk" size="sm" icon={indicator} />
        </span>
        <span className="status">
          {connected ? 'Connected' : 'Disconnected'}
        </span>
      </div>
    );
  }

  passwordChange = e => {
    clearTimeout(this.credsTimeout);
    e.preventDefault();
    const password = e.target.value;
    this.password = password;
    this.credsTimeout = setTimeout(this.credsChange, 1500);
  };

  emailChange = e => {
    clearTimeout(this.credsTimeout);
    e.preventDefault();
    const email = e.target.value;
    this.email = email;
    this.credsTimeout = setTimeout(this.credsChange, 1500);
  };

  delayChange = e => {
    const delay = e.target.value >= 0 ? e.target.value : 0;
    this.setState({ delay });
    HueService.setDelay(delay);
  };

  durationChange = e => {
    const duration = e.target.value >= 0 ? e.target.value : 0;
    this.setState({ duration });
    HueService.setDuration(duration);
  };

  credsChange = () => {
    goalfeedService.setCredentials(this.email, this.password);
  };

  render() {
    return (
      <div>
        <h1>Settings</h1>
        <h2>Credentials</h2>
        <form>
          <div>
            <label>
              Email{' '}
              <input
                type="text"
                onChange={this.emailChange}
                placeholder={this.state.initialEmail}
              />
            </label>
          </div>
          <div>
            <label>
              Password{' '}
              <input
                type="password"
                onChange={this.passwordChange}
                placeholder="Your super secret password"
              />
            </label>
          </div>
        </form>
        {this.renderConnectedIndicator()}
        <h2>Goal Settings</h2>
        <div>
          <label>
            Delay (seconds)
            <input
              type="number"
              value={this.state.delay}
              onChange={this.delayChange}
            />
          </label>
        </div>
        <div>
          <label>
            Duration (seconds)
            <input
              type="number"
              value={this.state.duration}
              onChange={this.durationChange}
            />
          </label>
        </div>
        <h2>Test Area</h2>
        <button onClick={HueService.goal}>Test Goal</button>

        <Feed></Feed>
      </div>
    );
  }
}

export default Settings;
