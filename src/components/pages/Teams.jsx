import React, { Component } from 'react';
import * as TeamService from '../../modules/TeamService';
import Checkbox from '../partials/Checkbox.jsx';

import {
  faHockeySticks,
  faBaseball,
  faFutbol,
} from '@fortawesome/pro-light-svg-icons';

import {
  faHockeySticks as faHockeySticksSolid,
  faBaseball as faBaseballSolid,
  faFutbol as faFutbolSolid,
} from '@fortawesome/pro-solid-svg-icons';

export class Teams extends Component {
  state = {
    leagues: [],
    selectedTeams: [],
  };
  constructor() {
    super();

    this.init();
  }

  async init() {
    this.setState({ selectedTeams: await TeamService.getSelectedTeams() });
    this.populateLeagues();
  }

  getIcon(leagueId, checked = false) {
    const NHL_ID = 1;
    const MLB_ID = 2;
    const EPL_ID = 3;

    if (leagueId === NHL_ID) {
      return checked ? faHockeySticksSolid : faHockeySticks;
    } else if (leagueId === MLB_ID) {
      return checked ? faBaseballSolid : faBaseball;
    } else if (leagueId === EPL_ID) {
      return checked ? faFutbolSolid : faFutbol;
    }
  }

  async populateLeagues() {
    const leagues = await TeamService.getAllLeagues();
    this.setState({ leagues });

    console.log(leagues);
  }

  toggleTeamSelection = e => {
    const team_hash = String(e.target.value);
    const selectedTeams = e.target.checked
      ? [...this.state.selectedTeams, team_hash]
      : this.state.selectedTeams.filter(val => {
          return val !== team_hash;
        });
    TeamService.setSelectedTeams(selectedTeams);
    this.setState({ selectedTeams: selectedTeams });
  };

  isTeamSelected(team_hash) {
    return this.state.selectedTeams.includes(team_hash);
  }

  render() {
    return (
      <div>
        <h1>Teams</h1>
        {this.state.leagues.map((league, i) => {
          return (
            <div key={i}>
              <h2>{league.name}</h2>
              <form className="frmLightSelect">
                {league.teams.map((team, i) => {
                  return (
                    <Checkbox
                      key={i}
                      id={team.team_hash}
                      text={team.team_name}
                      checked={this.isTeamSelected(team.team_hash)}
                      onChange={this.toggleTeamSelection}
                      checkedIcon={this.getIcon(team.league_id, true)}
                      unCheckedIcon={this.getIcon(team.league_id)}
                    />
                  );
                })}
              </form>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Teams;
