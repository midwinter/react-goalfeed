import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faLightbulbSlash,
  faLightbulbOn,
} from '@fortawesome/pro-light-svg-icons';
import * as HueService from '../../../modules/HueService';
import './lightselect.scss';

export class Config extends Component {
  state = {
    lights: [],
    selectedLights: [],
  };
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.init();
  }
  init = async () => {
    const lights = await HueService.getLights();
    const selectedLights = await HueService.getGoalfeedGroupLightIds();
    this.setState({
      lights: lights,
      selectedLights: selectedLights,
    });

    console.log(this.state);
  };
  selectionChange = e => {
    const lightId = String(e.target.value);
    const selectedLights = e.target.checked
      ? [...this.state.selectedLights, lightId]
      : this.state.selectedLights.filter(val => {
          return val !== lightId;
        });
    this.setState({ selectedLights: selectedLights });
    HueService.setGoalfeedGroup(selectedLights);
  };
  isLightSelected(lightId) {
    return this.state.selectedLights.includes(lightId);
  }
  getCheckboxName(lightId) {
    return 'chkLight' + lightId;
  }
  disconnectBridge() {
    HueService.disconnectBridge();
    this.props.onDisconnect();
  }
  render() {
    return (
      <div>
        <p>Select lights from the list below to use in the goal light</p>
        <form className="frmLightSelect">
          {this.state.lights.map((light, i) => {
            return (
              <label
                key={i}
                className={
                  'chkLight' +
                  (this.isLightSelected(light.id) ? '-selected' : '')
                }
              >
                <input
                  type="checkbox"
                  onChange={this.selectionChange}
                  checked={this.isLightSelected(light.id)}
                  id={this.getCheckboxName(light.id)}
                  value={light.id}
                />
                <FontAwesomeIcon
                  className="icoLightChk"
                  size="lg"
                  icon={
                    this.isLightSelected(light.id)
                      ? faLightbulbOn
                      : faLightbulbSlash
                  }
                />
                {light.name}
              </label>
            );
          })}
        </form>
        <h2>Disconnect Bridge</h2>
        <div>
          By pressing this button, you will disconnect the bridge and need to
          re-pair
        </div>
        <button onClick={this.disconnectBridge}>Disconnect</button>
      </div>
    );
  }
}

export default Config;
