import React, { Component } from 'react';

import Searching from './AuthSteps/Searching';
import Found from './AuthSteps/Found';

export class Authorize extends Component {
  bridgeSelected = bridgeIp => {
    this.setState({ adding: bridgeIp, auth_step: 'ready' });
    console.log(bridgeIp);
  };
  pairingSuccess = () => {
    this.props.onSuccess();
  };

  bridgesDiscovered = bridges => {
    console.log(bridges);
    this.setState({ bridges: bridges, auth_step: 'found' });
  };

  getAuthStep() {
    if (this.state.auth_step === 'searching') {
      return <Searching onDiscover={this.bridgesDiscovered} />;
    }

    if (this.state.auth_step === 'found') {
      return (
        <Found
          bridges={this.state.bridges}
          bridgeSelected={this.bridgeSelected}
          pairingSuccess={this.pairingSuccess}
        />
      );
    }
  }
  state = {
    bridges: [],
    loaded: false,
    bridgeToAdd: false,
    discovering: false,
    selectedTeams: [],
    pairingStarted: false,
    auth_step: 'searching',
  };
  render() {
    return <div>{this.getAuthStep()}</div>;
  }
}

export default Authorize;
