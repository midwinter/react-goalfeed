import React, { Component } from 'react';

export class PairForm extends Component {
  state = {
    bridges: [],
    manualIp: '',
  };
  componentDidMount() {
    this.setState({ bridges: this.props.bridges });
  }
  getBridgeList() {
    if (this.state.bridges.length > 0) {
      return (
        <React.Fragment>
          <h3>Discovered Bridges</h3>
          <div className="bridgeList">
            {this.state.bridges.map((bridge, index) => {
              return (
                <button
                  key={index}
                  className="pageButton"
                  onClick={e => {
                    this.props.onPair(bridge.ip);
                  }}
                >
                  {bridge.ip}
                </button>
              );
            })}
          </div>
        </React.Fragment>
      );
    }

    return <h3>No Discovered Bridges</h3>;
  }
  manualPair = e => {
    e.preventDefault();
    this.props.onPair(this.state.manualIp);
  };
  textChange = e => {
    this.setState({ manualIp: e.target.value });
  };
  getErrorMsg = () => {
    return this.props.errorMsg ? (
      <div className="errorMsg errorMsg-fade">{this.props.errorMsg}</div>
    ) : (
      ''
    );
  };
  render() {
    return (
      <div>
        <h2>Connect to Bridge</h2>
        <p>
          Press the link button at the top of your Hue bridge and then select a
          bridge from the list below
        </p>
        {this.getBridgeList()}

        <h3>Manual Entry</h3>
        <p>
          If your bridge does not appear in the list below, hit the pair button
          on your bridge and enter the IP address or hostname of your Hue Bridge
        </p>

        <form className="frmManualEntry" onSubmit={this.manualPair}>
          <span>
            <input type="text" onChange={this.textChange} />
            <button type="submit">Add</button>
          </span>
        </form>
        {this.getErrorMsg()}
      </div>
    );
  }
}

export default PairForm;
