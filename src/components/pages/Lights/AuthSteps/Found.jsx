import React, { Component } from 'react';
import './found.scss';
import * as HueService from '../../../../modules/HueService';
import huejay from 'huejay';
import PairForm from './partials/PairForm';

export class Found extends Component {
  state = {
    bridges: [],
    pairingStarted: false,
    errorMsg: '',
  };

  authorizeBridge = async ip => {
    this.setState({ pairingStared: true });
    let client = new huejay.Client({
      host: ip,
    });

    try {
      this.setState({
        errorMsg: '',
      });
      let user = new client.users.User();
      user = await client.users.create(user);
      console.log(`New user created - Username: ${user.username}`);
      HueService.setBridgeUser(user);
      HueService.setBridgeIp(ip);
      this.props.pairingSuccess();
    } catch (error) {
      this.setState({ pairingStarted: false });
      if (error instanceof huejay.Error && error.type === 101) {
        this.setState({
          errorMsg: 'Link button not pressed. Try again...',
        });
      } else {
        this.setState({
          errorMsg: JSON.stringify(error.message),
        });
      }
    }
  };

  render() {
    return this.state.pairingStarted ? (
      'Pairing in progress'
    ) : (
      <PairForm
        bridges={this.props.bridges}
        onPair={this.authorizeBridge}
        errorMsg={this.state.errorMsg}
      />
    );
  }
}

export default Found;
