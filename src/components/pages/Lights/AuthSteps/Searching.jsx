import React, { Component } from 'react';
import huejay from 'huejay';

export class Searching extends Component {
  state = {
    searching: true,
    noBridges: false,
  };
  discover = async () => {
    console.log('discovering');
    this.setState({ searching: true });
    try {
      const bridges = await huejay.discover({ strategy: 'nupnp' });

      this.discoverComplete(bridges);
    } catch (e) {
      console.log(e);
    }
    this._isMounted && this.setState({ searching: false });
  };
  discoverComplete(bridges) {
    if (bridges.length > 0) {
      this.props.onDiscover(bridges);
    }
  }
  getStatusText() {
    if (this.state.searching) {
      return 'Looking for bridges';
    }
  }

  constructor() {
    super();
    this._isMounted = false;
  }
  componentDidMount() {
    this._isMounted = true;
    this.discover();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    return (
      <div>
        <h2>Authorize Bridge</h2>

        {this.getStatusText()}
      </div>
    );
  }
}

export default Searching;
