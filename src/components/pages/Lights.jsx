import React, { Component } from 'react';
import * as HueService from '../../modules/HueService';
import Authorize from './Lights/Authorize';
import Config from './Lights/Config';

export class Lights extends Component {
  state = {
    can_connect: false,
    loaded: false,
    selectedLights: [],
    lights: [],
  };
  constructor() {
    super();
    this._isMounted = false;
  }
  selectionChange() {
    HueService.setGoalfeedGroup(this.selectedLights);
  }
  componentDidMount() {
    this._isMounted = true;
    this.reinit();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  reinit = async () => {
    const can_connect = await HueService.connectionIsSetUp();

    this._isMounted && this.setState({ can_connect: can_connect });
  };

  onPairSuccess() {
    this.reinit();
  }

  configPage() {
    if (this.state.can_connect) {
      return (
        <Config
          lights={this.state.lights}
          selectedLights={this.state.selectedLights}
          onDisconnect={this.reinit}
        />
      );
    } else {
      return <Authorize onSuccess={this.reinit} />;
    }
  }
  render() {
    return (
      <div>
        <h1>Lights Config</h1>
        {this.configPage()}
      </div>
    );
  }
}

export default Lights;
