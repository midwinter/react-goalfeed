import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class MenuItem extends Component {
  render() {
    return (
      <Link to={this.props.to}>
        <li className="menuItem">
          <i
            className={'fa fa-' + this.props.icon}
            style={{ margin: '0 .5em' }}
          />{' '}
          {this.props.itemText}
        </li>
      </Link>
    );
  }
}

export default MenuItem;
