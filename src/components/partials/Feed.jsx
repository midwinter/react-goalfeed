import React from 'react';
import Moment from 'react-moment';
import GoalfeedService from '../../modules/GoalfeedService';
const goalfeedService = GoalfeedService.getInstance();

class Feed extends React.Component {
  state = {
    goals: [],
  };
  constructor() {
    super();
    goalfeedService.on('goal', this.onGoal);
  }
  onGoal = data => {
    const goal = { time: Date.now(), ...data };
    const goals = [goal, ...this.state.goals];
    this.setState({ goals: goals });
  };
  render() {
    return (
      <div>
        {this.state.goals.map((goal, i) => {
          return (
            <div key={i}>
              Goal: {goal.team_name}{' '}
              <em>
                (<Moment fromNow>{goal.time}</Moment>)
              </em>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Feed;
