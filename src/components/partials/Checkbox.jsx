import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquare, faCheckSquare } from '@fortawesome/pro-light-svg-icons';
import './checkbox.scss';
import React from 'react';

export class Checkbox extends React.Component {
  getIcon() {
    if (this.props.checked) {
      return this.props.checkedIcon ? this.props.checkedIcon : faCheckSquare;
    }
    return this.props.unCheckedIcon ? this.props.unCheckedIcon : faSquare;
  }
  render() {
    return (
      <label className={'chkBox' + (this.props.checked ? '-selected' : '')}>
        <input
          type="checkbox"
          onChange={this.props.onChange}
          checked={this.props.checked}
          id={this.props.id}
          value={this.props.id}
        />
        <FontAwesomeIcon className="icoChk" size="lg" icon={this.getIcon()} />
        {this.props.text}
      </label>
    );
  }
}

export default Checkbox;
