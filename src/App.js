import React from 'react';
import './App.scss';
import Menu from './components/Menu';
import '@fortawesome/fontawesome-pro/css/all.css';
import '@fortawesome/fontawesome-pro/js/all.js';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Lights from './components/pages/Lights';
import Teams from './components/pages/Teams';
import Settings from './components/pages/Settings';
import GoalfeedService from './modules/GoalfeedService';
import * as HueService from './modules/HueService';
import * as TeamService from './modules/TeamService';

const goalfeedService = GoalfeedService.getInstance();

goalfeedService.on('goal', async data => {
  const selectedTeams = await TeamService.getSelectedTeams();
  if (!selectedTeams) return;
  for (const team of selectedTeams) {
    if (team === data.team_hash) {
      HueService.goal();
    }
  }
});

function App() {
  return (
    <Router>
      <div className="App">
        <Menu />

        <div className="mainContainer">
          <Route path="/lights" component={Lights} />
          <Route path="/teams" component={Teams} />
          <Route path="/" exact={true} component={Settings} />
        </div>
      </div>
    </Router>
  );
}

export default App;
